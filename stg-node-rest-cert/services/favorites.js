const {favorites} = require("../db");


function validateChange(oldValue, newValue) {
    if (newValue === undefined) return false;
    if (newValue === '') return false;
    return newValue !== oldValue;
}

async function getFavorite(id) {
    const fav = favorites.getFavorites().filter(fav => { return fav.id === parseInt(id) })[0];
    if (fav === undefined) return undefined;

    const count = favorites.getFavorites().filter(f => { return f.link === fav.link }).length;
    return { fav, count };
}

async function getUserFavorites(id) {
    return favorites.getFavorites().filter(fav => { return fav.userId === parseInt(id) });
}

async function createFavorite(fav, userId) {
    fav.id = favorites.length + 1;
    fav.userId = userId
    favorites.insertFavorite(fav);
}

async function updateFavorite(data, id, userId) {
    const favs = favorites.getFavorites();
    const index = favs.findIndex(fav => { return fav.id === parseInt(id) && fav.userId === parseInt(userId) });
    if (index === -1) {
        throw new Error("does not exist");
    }

    const title = validateChange(favs[index].title, data.title) ? data.title : favs[index].title;
    const link = validateChange(favs[index].link, data.link) ? data.link : favs[index].link;

    return favorites.updateFavorite(index, { title, link });
}

async function deleteFavorite(id, userId) {
    const index = favorites.getFavorites().findIndex(fav => { return fav.id === parseInt(id) && fav.userId === parseInt(userId)});
    if (index === -1) {
        throw new Error("does not exist");
    }

    return favorites.deleteFavorite(index);
}


module.exports ={
    getFavorite,
    getUserFavorites,
    createFavorite,
    updateFavorite,
    deleteFavorite
}