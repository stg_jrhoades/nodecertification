const { users } = require("../db");
const bcrypt = require("bcrypt");

function validateChange(oldValue, newValue) {
    if (newValue === undefined) return false;
    if (newValue === '') return false;
    return newValue !== oldValue;
}

async function getUsers() {
    return users.getUsers().map(user => {
        return {
            id: user.id,
            username: user.username,
            email: user.email
        }
    });
}

async function getUserById(id) {
    const user = users.getUsers().filter(user => { return user.id === parseInt(id) })[0];
    if (user === undefined) {
        throw new Error("unable to find user");
    }
    return {
        id: user.id,
        username: user.username,
        email: user.email
    };
}

async function createUser(user) {
    const saltRounds = parseInt(process.env.SALT_ROUNDS);
    let hash;
    try {
        hash = bcrypt.hashSync(user.password, saltRounds);
    } catch (err) {
        await Promise.reject(err);
    }

    // apply hash as new password
    user.password = hash;

    // Add to "database"
    return users.insertUser(user);
}

async function updateUser(user, reqUser) {
    // Update non hashed fields
    user.username = validateChange(user.username, reqUser.username) ? reqUser.username : user.username;
    user.email = validateChange(user.email, reqUser.email) ? reqUser.email : user.email;

    // Update hashed field
    let result;
    try {
        result = bcrypt.compareSync(reqUser.password, user.password);
    } catch (err) {
        await Promise.reject(err);
    }

    // Password is being changed
    if(!result) {
        deleteUserById(user.id).then(() => {
            user.password = reqUser.password;
            createUser(user)
        }).catch(err => { throw err });
    }
}

async function deleteUserById(id) {
    const index = users.getUsers().findIndex(user => { return user.id === parseInt(id) });
    if (index === -1) {
        throw new Error("does not exist");
    }

    users.deleteUser(index);
}

module.exports = {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUserById
}