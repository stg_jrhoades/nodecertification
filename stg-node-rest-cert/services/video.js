const { comments } = require("../db");

function validateChange(oldValue, newValue) {
    if (newValue === undefined) return false;
    if (newValue === '') return false;
    return newValue !== oldValue;
}

async function getComments(id) {
    return comments.getComments().filter(comment => { return comment.favId === parseInt(id)});
}

async function createComment(id, comment) {
    const com = {
        id: comments.length + 1,
        favId: parseInt(id),
        comment
    }

    comments.insertComment(com);
}

async function updateComment(id, commentId, comment ) {
    const coms = comments.getComments();
    const index =  coms.findIndex(com => { return com.id === parseInt(commentId) && com.favId === parseInt(id) });
    if (index === -1) {
        throw new Error("does not exist");
    }

    const value = validateChange(coms[index].comment, comment) ? comment : coms[index].comment;
    comments.updateComment(index, value);
}

async function deleteComment(id, commentId) {
    const index = comments.getComments().findIndex(com => { return com.id === parseInt(commentId) && com.favId === parseInt(id) });
    if (index === -1) {
        throw new Error("does not exist");
    }

    comments.deleteComment(index);
}


module.exports = {
    getComments,
    createComment,
    updateComment,
    deleteComment
}