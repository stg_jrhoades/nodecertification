# Documentation

## Prepping the application

    npm install

## Testing

    npm run test

## Running application "devlopment"
hot reload through nodemon is active

    npm run dev


## Running application "production"

    npm start

# Routes

## Auth `/auth`

method: `POST`

response:

    text: 'JWT auth token'

## Users `/users`

All the calls require an auth token.

### Get Users
method: `GET`

uri: `/`

response:

    [
        {
            id: 1,
            username: "jrhoades93",
            email: "justin.rhoades@stgconsulting.com"
        },
        {
            id: 2,
            username: "tony$tark",
            email: "tstark@example.com"
        },
        {
            id: 3,
            username: "captainS",
            email: "srogers@example.com"
        }
    ]

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Get User
method: `GET`

uri: `/:id`

response:

    {
        id: 1,
        username: "jrhoades93",
        email: "justin.rhoades@stgconsulting.com"
    }

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Create User
method: `POST`

uri: `/:id`

request:

    {
        id: 1,
        username: "jrhoades93",
        password: "something secret"
        email: "justin.rhoades@stgconsulting.com"
    }

response:

    {
        id: 1,
        username: "jrhoades93",
        password: ""
        email: "justin.rhoades@stgconsulting.com"
    }

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Update User
method: `PUT`

uri: `/:id`

request:

    {
        id: 1,
        username: "jrhoades93",
        password: "some new secret"
        email: "justin.rhoades@stgconsulting.com"
    }

response:

    {
        id: 1,
        username: "jrhoades93",
        password: ""
        email: "justin.rhoades@stgconsulting.com"
    }

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Delete User
method: `DELETE`

uri: `/:id`

response:

    {
        message: "delete successful",
    }

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

## Favorites `/favorites`

All the calls require an auth token.

### Get Favorites by User
method: `GET`

uri: `/`


response:

    [
        {
            id: 1,
            title: "Top 10 BS",
            link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
        },
        {
            id: 2,
            title: "Something dropping from very high",
            link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
        },
    ]

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Get Favorites by favorite Id
method: `GET`

uri: `/:favoriteId`


response:

    [
        {
            id: 1,
            title: "Top 10 BS",
            link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
        }
    ]

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Get Favorites by User
method: `GET`

uri: `/user/:userId`


response:

    [
        {
            id: 3,
            title: "Top 10 BS",
            link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
        }
    ]

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Create Favorite
method: `POST`

uri: `/`

request:

    {
        title: "some video title",
        link: "some link to video"
    }

response:

    status code: 200

error:
    
    status code: 400
    
    body: {
        error: "bad data"
    }

### Update Favorite
method: `PUT`

uri: `/:favoriteId`

request:

    {
        title: "some video title",
        link: "some link to video"
    }

response:

    status code: 200

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Delete Favorite
method: `DELETE`

uri: `/:favoriteId`

response:

    {
        message: "delete successful",
    }

error:

    status code: 400
    
    body: {
        error: "bad data"
    }


## Video/Comments `/video`

All the calls require an auth token.

### Get Video by User and favorite id
method: `GET`

uri: `/:favoriteId`


response:

    {
        video: {
            favs: {
                id: 1,
                title: "Top 10 BS",
                link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
            },
            count: 2
        },
        comments: [
            {
                comment: "Some comment"
            },
            ...
        ]
    }

error:

    status code: 400
    
    body: {
        error: "bad data"
    }


### Create Comment for a favorite video
method: `POST`

uri: `/:favoriteId/comment`

request:

    {
        comment: "some comment",
    }

response:

    status code: 200

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Update Comment for favorite video
method: `PUT`

uri: `/:favoriteId/comment/:commentId`

request:

    {
        title: "some video title",
        link: "some link to video"
    }

response:

    status code: 200

error:

    status code: 400
    
    body: {
        error: "bad data"
    }

### Delete Comment
method: `DELETE`

uri: `/:favoriteId/comment/:commentId`

response:

    {
        message: "delete successful",
    }

error:

    status code: 400
    
    body: {
        error: "bad data"
    }