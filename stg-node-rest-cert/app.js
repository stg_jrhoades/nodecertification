require("dotenv").config();
let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
const jwt = require("jsonwebtoken");

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let favoritesRouter = require('./routes/favorites');
let videoRouter = require('./routes/video');
const {users} = require("./db");

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// Applied plugins
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Auth Route
app.post('/auth', function (req, res, next) {
  if (!req.body) {
    console.error("bad body");
    res.sendStatus(401);
    return;
  }

  const user = users.getUsers().filter(usr => {return req.body.username === usr.username && req.body.password === usr.password })[0];
  if (!user) {
    console.error("no user");
    res.sendStatus(401);
    return;
  }

  if (req.body.username && req.body.password) {
    const token = jwt.sign({id: user.id, username: user.username}, process.env.TOKEN_SECRET, { expiresIn: '7d' });
    res.send(token);
  } else {
    res.sendStatus(401);
  }
})

// Route Groups
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/favorites', favoritesRouter)
app.use('/video', videoRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
