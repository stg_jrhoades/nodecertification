const { getUsers, createUser, updateUser, deleteUserById, getUserById} = require( "../services/users");
const {users, favorites, comments} = require("../db");
const {getFavorite, getUserFavorites, createFavorite, updateFavorite, deleteFavorite} = require("../services/favorites");
const {getComments, createComment, updateComment, deleteComment} = require("../services/video");

jest.mock("../db.js", () => {
    return {
        users: {
            getUsers: jest.fn(() => { return [
                {
                    id: 1,
                    username: "jrhoades93",
                    password: "secret",
                    email: "justin.rhoades@stgconsulting.com"
                },
                {
                    id: 2,
                    username: "tony$tark",
                    password: "$2b$10$kFSqeRKrBHodd2YqrkhLGucYW/mQMBPraHZP8eS9iIUIwgvmg7w4i",
                    email: "tstark@example.com"
                },
                {
                    id: 3,
                    username: "captainS",
                    password: "secret3",
                    email: "srogers@example.com"
                }
            ]}),
            insertUser: jest.fn((user) => { return [
                {
                    id: 1,
                    username: "jrhoades93",
                    password: "secret",
                    email: "justin.rhoades@stgconsulting.com"
                },
                {
                    id: 2,
                    username: "tony$tark",
                    password: "secret2",
                    email: "tstark@example.com"
                },
                {
                    id: 3,
                    username: "captainS",
                    password: "secret3",
                    email: "srogers@example.com"
                }
            ].push(user) }),
            deleteUser: jest.fn((index) => { [
                {
                    id: 1,
                    username: "jrhoades93",
                    password: "secret",
                    email: "justin.rhoades@stgconsulting.com"
                },
                {
                    id: 2,
                    username: "tony$tark",
                    password: "secret2",
                    email: "tstark@example.com"
                },
                {
                    id: 3,
                    username: "captainS",
                    password: "secret3",
                    email: "srogers@example.com"
                }
            ].splice(index, 1) })
        },
        favorites: {
            getFavorites: jest.fn(() => { return [
                {
                    id: 1,
                    userId: 1,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 2,
                    userId: 1,
                    title: "Something dropping from very high",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 3,
                    userId: 2,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                }
            ] }),
            insertFavorite: jest.fn((fav) => { return [
                {
                    id: 1,
                    userId: 1,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 2,
                    userId: 1,
                    title: "Something dropping from very high",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 3,
                    userId: 2,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                }
            ].push(fav) }),
            updateFavorite: jest.fn((index, { title, link }) => {
                const favs = [
                {
                    id: 1,
                    userId: 1,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 2,
                    userId: 1,
                    title: "Something dropping from very high",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 3,
                    userId: 2,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                }
            ];
                favs[index].title = title;
                favs[index].link = link;
                return favs;
            }),
            deleteFavorite: jest.fn((index) => { return [
                {
                    id: 1,
                    userId: 1,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 2,
                    userId: 1,
                    title: "Something dropping from very high",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 3,
                    userId: 2,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                }
            ].splice(index, 1) })
        },
        comments: {
            getComments: jest.fn(() => { return [
                {
                    id: 1,
                    favId: 1,
                    comment: "This video is so good"
                },
                {
                    id: 2,
                    favId: 1,
                    comment: "Good list would've added X"
                },
                {
                    id: 3,
                    favId: 2,
                    comment: "Drop a wrecking ball next time"
                },
                {
                    id: 4,
                    favId: 2,
                    comment: "How did come up with this"
                },
            ] }),
            insertComment: jest.fn((comment) => { return [
                {
                    id: 1,
                    favId: 1,
                    comment: "This video is so good"
                },
                {
                    id: 2,
                    favId: 1,
                    comment: "Good list would've added X"
                },
                {
                    id: 3,
                    favId: 2,
                    comment: "Drop a wrecking ball next time"
                },
                {
                    id: 4,
                    favId: 2,
                    comment: "How did come up with this"
                },
            ].push(comment) }),
            updateComment: jest.fn((index, comment) => {
                const coms = [
                    {
                        id: 1,
                        favId: 1,
                        comment: "This video is so good"
                    },
                    {
                        id: 2,
                        favId: 1,
                        comment: "Good list would've added X"
                    },
                    {
                        id: 3,
                        favId: 2,
                        comment: "Drop a wrecking ball next time"
                    },
                    {
                        id: 4,
                        favId: 2,
                        comment: "How did come up with this"
                    },
                ];
                coms[index].comment = comment;
                return coms;
            }),
            deleteComment: jest.fn((index) => { [
                {
                    id: 1,
                    favId: 1,
                    comment: "This video is so good"
                },
                {
                    id: 2,
                    favId: 1,
                    comment: "Good list would've added X"
                },
                {
                    id: 3,
                    favId: 2,
                    comment: "Drop a wrecking ball next time"
                },
                {
                    id: 4,
                    favId: 2,
                    comment: "How did come up with this"
                },
            ].splice(index, 1) })
        }
    }
})

describe("User Services", () => {
    test("Should get users", async () => {
        const data = await getUsers();
        expect(data.length).toBe(3);
    })

    test("should get users by id", async () => {
        const data = await getUserById(2);
        expect(data.username).toBe("tony$tark");
    })

    test("creating user", async () => {
        const data = {
            id: 5,
            username: "test",
            password: "testPass",
            email: "test@example.com"
        };

        await createUser(data).catch(err => { console.error(err.toString()) });
        expect(users.insertUser.mock.calls.length).toBe(1);
    })

    test("updates user", async () => {
        const testData = {
            id: 2,
            username: "tony$tark",
            password: "$2b$10$kFSqeRKrBHodd2YqrkhLGucYW/mQMBPraHZP8eS9iIUIwgvmg7w4i",
            email: "tstark@example.com"
        };
        const data = {
            id: 4,
            username: "test",
            password: "testPass2",
            email: "test@example.com"
        };

        await updateUser(testData, data);
        expect(users.deleteUser.mock.calls.length).toBe(1);
        expect(users.insertUser.mock.calls.length).toBe(2);
    })

    test("should delete user", async () => {
        await deleteUserById(3);
        expect(users.deleteUser.mock.calls.length).toBe(2);
    })
})

describe("Favorite Services", () => {
    it("should get favorites", async () => {
        const data = await getFavorite(1);
        expect(data.fav.title).toBe("Top 10 BS");
    })
    it("should get favorites for given user", async () => {
        const data = await getUserFavorites(1);
        expect(data.length).toBe(2);
    })
    it("should create favorites for given user", async () => {
        const newData = {
            title: "Test",
            link: "https://www.youtube.com/watch?v=TEST"
        };

        await createFavorite(newData, 4);
        expect(favorites.insertFavorite.mock.calls.length).toBe(1);
    })
    it("should update favorites for given user", async () => {
        const newData = {
            title: "Test",
            link: "https://www.youtube.com/watch?v=TEST"
        };

        await updateFavorite(newData, 1, 1);
        expect(favorites.updateFavorite.mock.calls.length).toBe(1);

    })
    it("should delete favorites for given user", async () => {
        await deleteFavorite(2, 1);
        expect(favorites.deleteFavorite.mock.calls.length).toBe(1);
    })
})

describe("Comments Services", () => {
    it("should get comment by favorite id", async () => {
        const data = await getComments(1);
        expect(data.length).toBe(2);
    })
    it("should create comments", async () => {
        await createComment(2, "This is test");
        expect(comments.insertComment.mock.calls.length).toBe(1);
    })
    it("should update comments", async () => {
        await updateComment(2, 3, "This is updated");
        expect(comments.updateComment.mock.calls.length).toBe(1);
    })
    it("should delete comments", async () => {
        await deleteComment(1, 1);
        expect(comments.deleteComment.mock.calls.length).toBe(1);
    })
})