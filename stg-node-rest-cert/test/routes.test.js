const request = require("supertest")
const app = require("../app");
const {getUserById, createUser, updateUser, deleteUserById} = require("../services/users");
const {getUserFavorites, createFavorite, updateFavorite, deleteFavorite, getFavorite} = require("../services/favorites");
const {createComment, updateComment, deleteComment, getComments} = require("../services/video");

jest.mock("../services/users", () => {
    return {
        getUsers: jest.fn(async () => {
            return Promise.resolve([
                {
                    id: 1,
                    username: "jrhoades93",
                    password: "secret",
                    email: "justin.rhoades@stgconsulting.com"
                },
                {
                    id: 2,
                    username: "tony$tark",
                    password: "secret2",
                    email: "tstark@example.com"
                },
                {
                    id: 3,
                    username: "captainS",
                    password: "secret3",
                    email: "srogers@example.com"
                }
            ])
        }),
        getUserById: jest.fn(async () => {}),
        createUser: jest.fn(async () => {}),
        updateUser: jest.fn(async () => {}),
        deleteUserById: jest.fn(async () => {}),
    }
});

jest.mock("../services/favorites", () => {
    return {
        getFavorite: jest.fn(async () => {
            return Promise.resolve({
                fav: {
                    id: 1,
                    userId: 1,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                count: 2
            })
        }),
        getUserFavorites: jest.fn(async () => {
            return Promise.resolve([
                {
                    id: 1,
                    userId: 1,
                    title: "Top 10 BS",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                },
                {
                    id: 2,
                    userId: 1,
                    title: "Something dropping from very high",
                    link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                }
            ])
        }),
        createFavorite: jest.fn(async () => {}),
        updateFavorite: jest.fn(async () => {}),
        deleteFavorite: jest.fn(async () => {}),
    }
});

jest.mock("../services/video", () => {
    return {
        getComments: jest.fn(async () => {}),
        createComment: jest.fn(async () => {}),
        updateComment: jest.fn(async () => {}),
        deleteComment: jest.fn(async () => {}),
    }
});

describe("Routes", () => {
    let token;

    // Auth test plus getting token for rest of the tests
    beforeAll(async () => {
        const res = await request(app)
            .post("/auth")
            .send({username: "jrhoades93", password: "secret"})

        expect(res.text).not.toBeNull();
        token = res.text;
    })


    describe("User Routes", () => {
        it("should return users", async () => {
            const res = await request(app)
                .get("/users/")
                .set("authorization", token);

            expect(res.statusCode).toEqual(200);
            expect(res.body.length).toEqual(3);
        })
        it("should return specific user", async () => {
            await request(app)
                .get("/users/1")
                .set("authorization", token);

            expect(getUserById.mock.calls.length).toEqual(1);
        })
        it("should create user", async () => {
            await request(app)
                .post("/users")
                .send({username: "jrhoades", password: "secret", email: "jrhoades@stgutah.com"})
                .set("authorization", token);

            expect(createUser.mock.calls.length).toEqual(1);
        })
        it("should update user", async () => {
            await request(app)
                .put("/users/1")
                .send({username: "jrhoades", password: "secret", email: "jrhoades@stgutah.com"})
                .set("authorization", token);

            expect(updateUser.mock.calls.length).toEqual(1);
        })
        it("should delete user", async () => {
            await request(app)
                .delete("/users/1")
                .set("authorization", token);

            expect(deleteUserById.mock.calls.length).toEqual(1);
        })
    })

    describe("Favorites Routes", () => {
        it("should return favorites", async () => {
            await request(app)
                .get("/favorites/")
                .set("authorization", token);

            expect(getUserFavorites.mock.calls.length).toEqual(1);
        })
        it("should return favorite", async () => {
            await request(app)
                .get("/favorites/1")
                .set("authorization", token);

            expect(getUserFavorites.mock.calls.length).toEqual(2);
        })
        it("should return favorites", async () => {
            await request(app)
                .get("/favorites/user/1")
                .set("authorization", token);

            expect(getUserFavorites.mock.calls.length).toEqual(3);
        })
        it("should create favorite", async () => {
            await request(app)
                .post("/favorites/")
                .send({title: "test", link: "testLink"})
                .set("authorization", token);

            expect(createFavorite.mock.calls.length).toEqual(1);
        })
        it("should update favorite", async () => {
            await request(app)
                .put("/favorites/1")
                .send({title: "test", link: "testLink"})
                .set("authorization", token);

            expect(updateFavorite.mock.calls.length).toEqual(1);
        })
        it("should delete favorite", async () => {
            await request(app)
                .delete("/favorites/1")
                .set("authorization", token);

            expect(deleteFavorite.mock.calls.length).toEqual(1);
        })
    })

    describe("Comments Routes", () => {
        it("should return comments", async () => {
            await request(app)
                .get("/video/1")
                .set("authorization", token);

            expect(getFavorite.mock.calls.length).toEqual(1);
            expect(getComments.mock.calls.length).toEqual(1);
        })
        it("should create comment", async () => {
            await request(app)
                .post("/video/1/comment")
                .send({comment: "test"})
                .set("authorization", token);

            expect(createComment.mock.calls.length).toEqual(1);
        })
        it("should update comment", async () => {
            await request(app)
                .put("/video/1/comment/1")
                .send({comment: "test"})
                .set("authorization", token);

            expect(updateComment.mock.calls.length).toEqual(1);
        })
        it("should delete comment", async () => {
            await request(app)
                .delete("/video/1/comment/1")
                .set("authorization", token);

            expect(deleteComment.mock.calls.length).toEqual(1);
        })
    })
})