const usersData = [
    {
        id: 1,
        username: "jrhoades93",
        password: "secret",
        email: "justin.rhoades@stgconsulting.com"
    },
    {
        id: 2,
        username: "tony$tark",
        password: "secret2",
        email: "tstark@example.com"
    },
    {
        id: 3,
        username: "captainS",
        password: "secret3",
        email: "srogers@example.com"
    }
];

const users = {
    getUsers: () => { return usersData },
    insertUser: (user) => {
        usersData.push(user);
        return usersData;
    },
    deleteUser: (index) => {
        usersData.splice(index, 1);
        return usersData;
    }
}

const favoriteData = [
    {
        id: 1,
        userId: 1,
        title: "Top 10 BS",
        link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
    },
    {
        id: 2,
        userId: 1,
        title: "Something dropping from very high",
        link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
    },
    {
        id: 3,
        userId: 2,
        title: "Top 10 BS",
        link: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
    }
];

const favorites = {
    getFavorites: () => { return favoriteData },
    insertFavorite: (fav) => {
        favoriteData.push(fav);
        return favoriteData;
    },
    updateFavorite: (index, {title, link}) => {
        favoriteData[index].title = title;
        favoriteData[index].link = link;
        return favoriteData;
    },
    deleteFavorite: (index) => {
        favoriteData.splice(index, 1);
        return favoriteData;
    }
}

const commentData = [
    {
        id: 1,
        favId: 1,
        comment: "This video is so good"
    },
    {
        id: 2,
        favId: 1,
        comment: "Good list would've added X"
    },
    {
        id: 3,
        favId: 2,
        comment: "Drop a wrecking ball next time"
    },
    {
        id: 4,
        favId: 2,
        comment: "How did come up with this"
    },
]

const comments = {
    getComments: () => { return commentData },
    insertComment: (comment) => { return commentData.push(comment) },
    updateComment: (index, comment) => {
        commentData[index].comment = comment
        return commentData
    },
    deleteComment: (index) => {
        commentData.splice(index, 1);
        return commentData;
    }
}

module.exports = {
    users,
    favorites,
    comments
}