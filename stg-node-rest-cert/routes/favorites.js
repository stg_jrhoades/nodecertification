const express = require("express");
const {authenticateToken} = require("../auth");
const {getUserFavorites, createFavorite, updateFavorite, deleteFavorite} = require("../services/favorites");
const router = express.Router();


// Utility function
function validFavorite(data) {
    if (data === undefined) return false;
    if (data.title === undefined) return false;
    if (data.link === undefined) return false;
    return true;
}


/* GET all favorites by user */
router.get('/', authenticateToken, function(req, res, next) {
    getUserFavorites(req.user.id)
        .then(favs => {
            res.send(favs.map(({id, title, link}) => { return { id, title, link } }));
        })
        .catch(err => {
            console.error(err);
            res.status(400).send({error: 'bad data'})
        })
})

/* GET favorite by favorite id */
router.get('/:id', authenticateToken, function(req, res, next) {
    getUserFavorites(req.user.id)
        .then(favs => {
            const fav = favs
                .filter(fav => { return fav.id === parseInt(req.params.id) })
                .map(({id, title, link}) => { return { id, title, link } })[0];
            res.send(fav);
        })
        .catch(err => {
            console.error(err);
            res.status(400).send({error: 'bad data'})
        })
})

/* GET all favorites by other user */
router.get('/user/:id', authenticateToken, function(req, res, next) {
    getUserFavorites(req.params.id)
        .then(favs => {
            res.send(favs.map(({title, link}) => { return { title, link } }));
        })
        .catch(err => {
            console.error(err);
            res.status(400).send({error: 'bad data'})
        })
})

/* POST favorite by user */
router.post('/', authenticateToken, function(req, res, next) {
    if (!validFavorite(req.body)) {
        console.error("failed validation")
        res.sendStatus(400);
        return;
    }

    const data = {
        title: req.body.title,
        link: req.body.link
    }

    createFavorite(data, req.user.id)
        .then(() => {
            res.sendStatus(200)
        })
        .catch(err => {
            console.error(err);
            res.status(400).send({error: 'bad data'})
        })
})

/* PUT favorite by user */
router.put('/:id', authenticateToken, function(req, res, next) {
    if (!validFavorite(req.body)) {
        console.error("failed validation")
        res.sendStatus(400);
        return;
    }

    const data = {
        title: req.body.title,
        link: req.body.link
    }

    updateFavorite(data, req.params.id, req.user.id)
        .then(() => {
            res.sendStatus(200)
        })
        .catch(err => {
            console.error(err);
            res.status(400).send({error: 'bad data'})
        })
})

/* DELETE favorite by user */
router.delete('/:id', authenticateToken, function(req, res, next) {
    deleteFavorite(req.params.id, req.user.id)
        .then(() => {
            res.sendStatus(200)
        })
        .catch(err => {
            console.error(err);
            res.status(400).send({error: 'bad data'})
        })
})


module.exports = router;