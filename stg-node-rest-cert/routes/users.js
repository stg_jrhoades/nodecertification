let express = require('express');
let bcrypt = require('bcrypt');
const {authenticateToken} = require("../auth");
let router = express.Router();

// Fake DB layer
const {getUsers, getUserById, createUser, deleteUserById, updateUser} = require("../services/users");
const {users} = require("../db");

// Utility Functions

function isValidUser(data) {
  if (data === undefined) throw new Error("no body");
  if (data.username === undefined) throw new Error("need username");
  if (data.email === undefined) throw new Error("need email");
  if (data.password === undefined) throw new Error("need password");
}

// Router functions

/* GET users listing. */
router.get('/', authenticateToken, function(req, res, next) {
  getUsers()
      .then(data => { res.send(data) })
      .catch(err => { res.status(500).send({error: err.toString()}) })
});

/* GET user by ID */
router.get('/:id', authenticateToken, function (req, res, next) {
  getUserById(req.params.id)
      .then(user => { 
        res.send(user); 
      })
      .catch(err => {
        console.error(err);
        res.status(400).send({error: 'bad data'})
      });
});

/* POST create user */
router.post('/', authenticateToken, function (req, res, next) {
  // Get and validate provided data
  const reqUser = req.body;
  isValidUser(reqUser);

  getUsers().then(users => {
    // Verify it's a new user
    if (users.some(user => { return user.email === reqUser.email })) {
      console.error('email already exists');
      res.status(400).send({error: 'bad data'});
      return;
    }

    // Sanctify the data
    const user = {
      id: users.length + 1,
      username: reqUser.username,
      password: reqUser.password,
      email: reqUser.email,
    };

    createUser(user).then(() => {
      // Cleanse the return data
      user.password = "";

      // return the approved user
      res.send(user);
    }).catch(err => {
      console.error(err);
      res.status(400).send({error: 'bad data'});
    });
  })
});

/* PUT update user */
router.put('/:id', authenticateToken, function (req, res, next) {
  try {
    // Get and validate provided data
    const reqUser = req.body;
    isValidUser(reqUser);

    getUsers().then(users => {
      // Verify it's not trying to take an already used email
      if (users.some(user => { return user.email === reqUser.email && user.id !== req.params.id }) || !users.some(user => { return user.id === parseInt(req.params.id) })) {
        throw new Error('email already exists');
      }

      users.forEach(user => {
        if (user.id === parseInt(req.params.id)) {
          updateUser(user, reqUser).catch(err => {
            throw new Error(err);
          });
        }
      })

      // Build a return
      const user = {
        id: parseInt(req.params.id),
        username: reqUser.username,
        password: "",
        email: reqUser.email,
      };

      // return the approved user
      res.send(user);
    });
  } catch (err) {
    res.status(400).send({error: err.toString()});
  }
});

/* DELETE user by ID */
router.delete('/:id', authenticateToken, function (req, res, next) {
  deleteUserById(req.params.id)
    .then(() => {
        res.send({ message: 'delete successful'});
    })
    .catch(err => {
      console.error(err);
      res.status(400).send({error: 'bad request'});
    })
});

module.exports = router;
