const express = require('express');
const {getUsers, getUserById, updateUser, createUser} = require("../services/users");
const {users} = require("../db");
const router = express.Router();

/* GET home page. */
router.get('/', async function(req, res, next) {
  const users = await getUsers();
  res.render(
      'index',
      { title: 'STG NodeJs Rest API', users }
  );
});

/* GET Login page */
router.get('/login', async function(req, res, next) {
  res.render('login');
})

/* GET Edit user page */
router.get('/edit/:id', async function(req, res, next) {
  const user = await getUserById(req.params.id);
  res.render('edit', { user });
})

router.post('/edit/:id', async function (req, res, next) {
  let message = 'saved';
  const data = req.body;
  data.id = req.params.id;

  const user = await getUserById(data.id);
  await updateUser(user, data).catch(err => {
    console.error(err.toString())
    message = 'Failed to save';
  });

  res.render('edit', { message });
})

/* GET Create page */
router.get('/create', async function(req, res, next) {
  res.render('create');
})

router.post('/create', async function(req, res, next) {
  const data = req.body
  const users = await getUsers();
  let message;

  if (!users.some(user => { return user.email === data.email })) {
    const user = {
      id: users.length + 1,
      username: data.username,
      password: data.password,
      email: data.email,
    };
    message = 'success';
    await createUser(user).catch(err => {
      console.error(err.toString());
      message = 'failed to save';
    })
  }

  res.render('create', { message });
})

module.exports = router;
