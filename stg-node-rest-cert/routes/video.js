const express = require("express");
const {authenticateToken} = require("../auth");
const { getFavorite } = require("../services/favorites");
const {getComments, createComment, updateComment, deleteComment} = require("../services/video");
const router = express.Router();


/* GET video by user and id */
router.get('/:id', authenticateToken, async function(req, res, next) {
    const video = await getFavorite(req.params.id);
    if (video === undefined) {
        res.sendStatus(400);
        return;
    }

    const comments = await getComments(video.fav.id);
    res.send({
        video,
        comments
    })
})

router.post('/:id/comment', authenticateToken, async function(req, res, next) {
    if (req.body.comment === undefined || req.body.comment === "") {
        res.sendStatus(400);
        return;
    }

    await createComment(req.params.id, req.body.comment);
    res.sendStatus(200);
})

router.put('/:id/comment/:commentId', authenticateToken, async function(req, res, next) {
    if (req.body.comment === undefined || req.body.comment === "") {
        res.sendStatus(400);
        return;
    }

    await updateComment(req.params.id, req.params.commentId, req.body.comment).catch(err => {
        res.sendStatus(400);
        return;
    })
    res.sendStatus(200);
})

router.delete('/:id/comment/:commentId', authenticateToken, async function(req, res, next) {
    await deleteComment(req.params.id, req.params.commentId).catch(err => {
        res.sendStatus(400);
        return;
    })
    res.sendStatus(200);
})



module.exports = router;