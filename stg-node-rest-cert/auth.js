let jwt = require("jsonwebtoken");

function authenticateToken(req, res, next) {
    const token = req.headers['authorization']

    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
        if (err) {
            console.error("Error", err);
            return res.sendStatus(403);
        }

        req.user = user;
        next()
    })
}

module.exports = { authenticateToken };