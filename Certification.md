# STG Spring Boot REST Certification

## About this Certification:

REST is not a technology by a particular organization.  It is an Architectural Style for defining constraints in building web services.  According to Roy Fielding’s dissertation, it is “The Architectural Style” of the modern web.

This means we cannot simply give you a link to “The Documentation” on REST as a single authoritative standard.

In this certification, we will provide instructions to setup a project that can be reviewed by a reviewer.  We provide references to tutorials and documentation to gain a basic understanding of REST and then a practical understanding of developing REST services within Node and express.

## Preliminaries

1. Fork the repository (https://bitbucket.org/stgconsulting/noderestcertification).
1. Contact through email or Slack the reviewer that that your project has been setup on your local machine and are ready to start the project.
1. Review the project problem: [Business Problem](https://bitbucket.org/stgconsulting/noderestcertification/src/master/BusinessProblem.md)
1. Follow the steps in the [README.md](./README.md) to complete the certification.
1. If you decide not to complete the project, or will need to step away from working on your project due to other time constraints, please inform the reviewer.

## Prerequisites
1. Core Node (learnyounod is a good place to start)
1. Express

## Key Points to Understand:

### REST Basics
* What is stateless web architecture?
* What makes a ***good*** (generally accepted) REST application?
* What is HTTP?
* What is a URL?
* What makes up an HTTP request?
* What makes up an HTTP response?
* Name all HTTP verbs and conventions on when they are used.
* Why do you want to use the proper HTTP response code with your response?
* What are the traits of a well named REST endpoint?
* What is marshalling?
* What is serialization?
* What is deserialization?

### Project Directory Structure
* Why is a good project directory structure helpful in developing code?  Why is it helpful for those who will review your code?
* We recommend the following [Directory Structure]
* Why should you use tools such as "express-generator"?

### Node and Express Basics
* What is Middleware?
* What is the signature of the standard node callback?
* How do you configure a .env file?
* What type of files do we put in the resources directory?
* How do you set the server port?
* How do you include required libraries into your project?


## Free Resources

[Roy Fieldings Dissertation](https://www.ics.uci.edu/~fielding/pubs/dissertation/top.htm) - Origination of "REST".

[Richardson Maturity Model](https://restfulapi.net/richardson-maturity-model/) - Defines "maturity" of a REST model/architecture.

[REST Api Tutorial](https://restfulapi.net/) - Good starting point.

[Baeldung](https://www.baeldung.com/) - Great for top the point and summary explanations.

[Code Geek](http://codegeek.net) - Great for explanations.

[Mkyong](http://mkyong.com) - Great for to the point examples.

[stackoverflow](https://stackoverflow.com/) - To the point answers - Post Questions

[REST naming](https://restfulapi.net/resource-naming/) - best practices

## Node Documentation

[Node js docks (chose your version here.)](https://nodejs.org/en/docs/)

[express docs](https://expressjs.com/)

[NVM](https://github.com/nvm-sh/nvm)

## Paid Resources

***Note:*** Some paid resources may be reimbursable. See the [STG Training Policy](https://docs.google.com/document/d/1rKuMBH8plra7Uv_MSsL_5BQrn4-EBlM78vpnk0ATp2I/edit).


